﻿using System;

namespace RgbConverter
{
#pragma warning disable
    public static class Rgb
    {
        /// <summary>
        /// Gets hexadecimal representation source RGB decimal values.
        /// </summary>
        /// <param name="red">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="green">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="blue">The valid decimal value for RGB is in the range 0-255.</param>
        /// <returns>Returns hexadecimal representation source RGB decimal values.</returns>
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            if (green < 0)
                green = 0;
            if (blue < 0)
                blue = 0;
            if (red < 0)
                red = 0;

            if (green > 255)
                green = 255;
            if (blue > 255)
                blue = 255;
            if (red > 255)
                red = 255;

            string hexRed = red.ToString("X2");
            string hexGreen = green.ToString("X2");
            string hexBlue = blue.ToString("X2");

            return hexRed + hexGreen + hexBlue;
        }
    }
}
